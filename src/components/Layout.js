import React from "react";
import Helmet from "react-helmet";

import Navbar from "../components/Navbar";
import "./all.sass";
import { Location } from "@reach/router";

const TemplateWrapper = ({ children }) => (
  <Location>
    {({ location }) => (
      <div>
        <Helmet title="Home | Gatsby + Netlify CMS" />
        {location.pathname}
        <Navbar />
        <div>{children}</div>
      </div>
    )}
  </Location>
);

export default TemplateWrapper;
