import React from "react";
import { renderToString } from "react-dom/server";

export const replaceRenderer = ({ bodyComponent, replaceBodyHTMLString }) => {
  const props = { test: "test" };
  const ConnectedBody = props => <>{bodyComponent}</>;
  replaceBodyHTMLString(renderToString(<ConnectedBody />));
};
